# Carwatch
## blockchain
This folder contains deployed smart contract **carwatch.sol** for Carwatch. It is written according to version 0.8.7 of solidity and contains follwing five functions.

- ##### isAllowed
    This function checks if user is allowed to make changes in smart contract data.

- ##### addUser
    it allows smart contract owner to add user to make changes.

- ##### view_AllowedAddresses
    To check which addresses are allowed to make changes in smart contract data

- ##### enterData
    Function used to enter new data on blockchain. only allowed addresses can do that.

- ##### getData
    Used to retreive data from blockchain
## python
This folder container python file **communicator.py** to communicate with smart contract and blockhcain. It has four functions to call subsequent functions in smart contract

- ##### Enter_Data
    This function uploads data to blockchain. It requires following parameters (in string format).
    -  Author Name (e.g., KimNixon)

    - Source (e.g., www.dodgedurango.net)
    - Publishing Date (e.g., 4/30/2020)
    - Breadcrump (e.g., dodgedurango.net)

    - Evaluation (e.g., Hater)

- ##### getData
    This function pulls all the data uploaded on blockchain. (No parameter needed)

- ##### view_AllowedAddresses
    Used to view blockchain addresses(evaluators) with permission to upload data to blockchain. (No parameter needed)

- ##### Enter_New_User
    Function used to add new evaluator to upload data. It requires blockchain address as input (e.g., 0x8EeA7B68705Fe4A9BACeccB53b071B7994C195e4)
## screenshots
This folder contains screenshots of different functionaities implementation results. A word file **description** is also included that provides further detail on each screenshot
