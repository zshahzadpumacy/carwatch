import sys, time
from web3 import Web3

blockchain_address = '---ENTER BLOCKCHAIN RPC URL---'
web3 = Web3(Web3.HTTPProvider(blockchain_address))

print("Connected to network:", web3.isConnected())

Sender_Address = Web3.toChecksumAddress("---ENTER PUBLIC ADDRESS HERE---")
Sender_Private_key = '---ENTER PRIVATE KEY HERE---'

Administration_contract_address = web3.toChecksumAddress('---ENTER CONTRACT ADDRESS HERE---')

abi = '[ { "anonymous": false, "inputs": [ { "indexed": true, "internalType": "address", "name": "previousOwner", "type": "address" }, { "indexed": true, "internalType": "address", "name": "newOwner", "type": "address" } ], "name": "OwnershipTransferred", "type": "event" }, { "inputs": [ { "internalType": "address", "name": "account", "type": "address" } ], "name": "isOwner", "outputs": [ { "internalType": "bool", "name": "", "type": "bool" } ], "stateMutability": "view", "type": "function", "constant": true }, { "inputs": [], "name": "owner", "outputs": [ { "internalType": "address", "name": "", "type": "address" } ], "stateMutability": "view", "type": "function", "constant": true }, { "inputs": [], "name": "renounceOwnership", "outputs": [], "stateMutability": "nonpayable", "type": "function" }, { "inputs": [ { "internalType": "address", "name": "newOwner", "type": "address" } ], "name": "transferOwnership", "outputs": [], "stateMutability": "nonpayable", "type": "function" }, { "inputs": [ { "internalType": "address", "name": "_user", "type": "address" } ], "name": "isAllowed", "outputs": [ { "internalType": "bool", "name": "_success", "type": "bool" } ], "stateMutability": "view", "type": "function", "constant": true }, { "inputs": [ { "internalType": "address", "name": "_User", "type": "address" } ], "name": "addUser", "outputs": [ { "internalType": "bool", "name": "_success", "type": "bool" } ], "stateMutability": "nonpayable", "type": "function" }, { "inputs": [], "name": "view_AllowedAddresses", "outputs": [ { "internalType": "address[]", "name": "Addresses", "type": "address[]" } ], "stateMutability": "view", "type": "function", "constant": true }, { "inputs": [ { "internalType": "string", "name": "_Author", "type": "string" }, { "internalType": "string", "name": "_Source", "type": "string" }, { "internalType": "string", "name": "_Published_date", "type": "string" }, { "internalType": "string", "name": "_Breadcrump", "type": "string" }, { "internalType": "string", "name": "_Evaluation", "type": "string" } ], "name": "enterData", "outputs": [ { "internalType": "bool", "name": "_success", "type": "bool" } ], "stateMutability": "nonpayable", "type": "function" }, { "inputs": [], "name": "getData", "outputs": [ { "components": [ { "internalType": "string", "name": "Author", "type": "string" }, { "internalType": "string", "name": "Source", "type": "string" }, { "internalType": "string", "name": "Published_date", "type": "string" }, { "internalType": "string", "name": "Breadcrump", "type": "string" }, { "internalType": "string", "name": "Evaluation", "type": "string" } ], "internalType": "struct CarWatch.data[]", "name": "", "type": "tuple[]" } ], "stateMutability": "view", "type": "function", "constant": true } ]'

contract = web3.eth.contract(address=Administration_contract_address, abi=abi)

    
def Enter_Data(Author_name, Source, Publishing_date, Breadcrump, Evaluation) :

    nonce = web3.eth.getTransactionCount(Sender_Address)
  
    txn_dict = contract.functions.enterData(Author_name,Source,Publishing_date,Breadcrump,Evaluation).buildTransaction({
        'gas': 500000,
        'gasPrice': web3.toWei('0', 'wei'),
        'nonce': nonce    
        })

    signed_txn = web3.eth.account.signTransaction(txn_dict, Sender_Private_key)

    txn_hash = web3.eth.sendRawTransaction(signed_txn.rawTransaction)

    print ("Waiting for transaction.")
    web3.eth.waitForTransactionReceipt(txn_hash)

    while(1):
        try:
            txn_receipt = web3.eth.getTransactionReceipt(txn_hash)
            flag = 1
        except:
            flag = 0
            e = sys.exc_info()
            print("Error getting Transaction Receipt:", e)
            print ("Trying again...")
        if flag==1:
            break
        time.sleep(2)

    print("Response:", txn_receipt)

def Enter_New_User(New_User_Address):
    nonce = web3.eth.getTransactionCount(Sender_Address)


    txn_dict = contract.functions.addUser(New_User_Address).buildTransaction({
        'gas': 500000,
        'gasPrice': web3.toWei('0', 'wei'),
        'nonce': nonce    
        })

    signed_txn = web3.eth.account.signTransaction(txn_dict, Sender_Private_key)

    txn_hash = web3.eth.sendRawTransaction(signed_txn.rawTransaction)

    print ("Waiting for transaction.")
    web3.eth.waitForTransactionReceipt(txn_hash)

    while(1):
        try:
            txn_receipt = web3.eth.getTransactionReceipt(txn_hash)
            flag = 1
        except:
            flag = 0
            e = sys.exc_info()
            print("Error getting Transaction Receipt:", e)
            print ("Trying again...")
        if flag==1:
            break
        time.sleep(2)

    print("Response:", txn_receipt)
        

print("################### MAIN")
while(True):
    command = input("Enter Command Number e.g. 2:\
          \n1. Enter Data\
           \n2. Get Data\
            \n3. View Allowed Addresses\
            \n4. Allow New User\n >>>")
    
    if not command.isdigit():
        print("??????? Entered Wrong Command")
        continue
    if command == "0":
        break

    elif command == "1":
        Author_name = input("Enter Author Name (e.g. KimNixon): \n >>>")
        Source = input("Enter Source (e.g. www.dodgedurango.net): \n >>>")
        Publishing_date = input("Enter Publishing date (e.g. 4/30/2020): \n >>>")
        Breadcrump = input("Enter Breadcrump (e.g. dodgedurango.net): \n >>>")
        Evaluation = input("Enter Evaluation (e.g. Hater): \n >>>")
        
        Enter_Data(Author_name, Source, Publishing_date, Breadcrump, Evaluation)  

    elif command == "2":
        data = contract.functions.getData().call()
        print(data)

    elif command == "3":
        Allowed_Addresses = contract.functions.view_AllowedAddresses().call()
        print(Allowed_Addresses)
        

    elif command == "4":
        New_User_Address = input("Enter New User Address (e.g. 0x8EeA7B68705Fe4A9BACeccB53b071B7994C195e4): \n >>>")
        if not web3.isChecksumAddress(New_User_Address):
            print("??????? Entered Wrong Address")
            break
        Enter_New_User(New_User_Address)
       

    
    