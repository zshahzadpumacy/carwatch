pragma solidity ^0.8.7;

import "./Ownable.sol";

contract CarWatch is Ownable{

    uint256 id;

    struct data{
        string Author;
        string Source;
        string Published_date;
        string Breadcrump; 
        string Evaluation;
    }

    mapping(uint256 => data) datas;

    address[] AllowedAddresses;

    modifier onlyAllowed{
         require(isAllowed(msg.sender) || isOwner(tx.origin), "not allowed");
        _;
    }

    function isAllowed(address _user) public view returns(bool _success){
        for (uint i = 0; i < AllowedAddresses.length; i++) {
            if (_user == AllowedAddresses[i]){
                return true;
            }
        }
        return false;
    }

    function addUser(address _User) public onlyOwner returns(bool _success){
        AllowedAddresses.push(_User);
        return true;
    }

    function view_AllowedAddresses() public view returns(address[] memory Addresses){
        return AllowedAddresses;
    }

    function enterData(string calldata _Author, string calldata _Source, string calldata _Published_date, string calldata _Breadcrump, string calldata _Evaluation) external onlyAllowed returns(bool _success){
        datas[id].Author = _Author;
        datas[id].Source = _Source;
        datas[id].Published_date = _Published_date;
        datas[id].Breadcrump = _Breadcrump;
        datas[id].Evaluation = _Evaluation;
        id++;
        return true;
    }

    function getData() public view returns(data[] memory){
        data[] memory ret = new data[](id);
        for (uint i = 0; i < id; i++) {
            ret[i] = datas[i];
        }
        return ret;
    }

}